global.include = function(file) {
    return require(abs_path('/' + file));
}

global.cleanText = function(file) {
    return 'Hello clean Text';
}

global.getError = function(obj, key) {
    for (prop in obj) {
        var newobj = obj[prop];
        for (prop2 in newobj) {
            if (prop2 == 'param' && newobj[prop2] == key) {
                return '<span class="text-danger">' + newobj['msg'] + '</span>';
            }
        }
    }
    return '';
}

global.getOld = function(obj, key, key2 = "", type = "") {
    for (prop in obj) {

        for (prop2 in obj[prop]) {
            if (prop == key && prop2 == key2) {
                return obj[prop][prop2];
            }
        }

        if (prop == key) {
            if (type == 'string')
                return String(obj[prop]);

            return obj[prop];

        }
    }
    return '';
}

global.getBulletinMonth = function(dt) {

    var month = new Array();
    month[0] = "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";

    var date = new Date(dt);
    var str = month[date.getMonth()] + ' ' + date.getFullYear();
    return str;
}


global.showDate = function(dt) {
    let dd = dt.getDate();

    let mm = dt.getMonth() + 1;
    const yyyy = dt.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }

    if (mm < 10) {
        mm = '0' + mm;
    }

    var fulldate = yyyy + '/' + mm + '/' + dd;

    return fulldate;
}