var express = require('express');
var router = express.Router();
const usersController = require('../controllers/api/UsersController');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express Application' });
});

module.exports = router;
