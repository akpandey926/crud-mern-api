var express = require('express');
var router = express.Router();
var UsersController  =  require('../controllers/api/UsersController');

router.all('/users/add', UsersController.add);
router.get('/test', UsersController.test);
router.get('/getAllUser', UsersController.getAllUser);
router.all('/users/edit/:user_id', UsersController.edit);
router.all('/users/view/:user_id', UsersController.getUser);
router.get('/users/delete/:user_id', UsersController.delete);

module.exports = router;
