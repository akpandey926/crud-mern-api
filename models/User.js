var mongoose = require('mongoose');
var uniqueValidator = require('mongoose-unique-validator');
slug = require('mongoose-slug-generator');
var Schema = mongoose.Schema;
const bcrypt = require('bcrypt');

var User = new Schema({
    name  : {type: String, required: false},
    slug: { type: String, slug: ["name"] },
    username   : {type: String, required: false},
    email :{type: String, required: false},
    phone :{type: Number, required: false},
    website :{type: String, required: false},
    status    : {type: String, required: false,enum : ['0', '1'], default: 1},
	},
	{
 		timestamps: { createdAt: 'created_at', updatedAt: 'updated_at'},
 		versionKey: false
	}
);
User.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.password);
}
User.pre('save', function(next) {
    var user = this;
    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();
    // hash the password using our new salt
    bcrypt.hash(user.password, 10, function(err, hash) {
        if (err) return next(err);
        // override the cleartext password with the hashed one
        user.password = hash;
        next();
    });
   
});
User.plugin(uniqueValidator);
mongoose.plugin(slug);
module.exports = mongoose.model('User', User);

